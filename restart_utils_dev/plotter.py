import matplotlib.pyplot as plt
import numpy as np

d = np.genfromtxt("./5_results.tsv")

plt.figure()

plt.plot(d[:,0], d[:,1], 'o')

plt.show()