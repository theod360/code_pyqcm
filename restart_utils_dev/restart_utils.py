import os
import sys

"""Argv passing convention

argv[1] ---> tsv name
argv[2] ---> index
argv[3 and onwards] ---> arguments to pass to `func` in `switcher`

"""


def restart_loop(filename, arg_list, tsv_name=None):
    ########################
    # Foolproof by detecting if there is a .py at the end... so os.system call can be a little more flexible
    ########################

    ########################
    # Foolproof by detecting if there is a .tsv at the end... so os.system call can be a little more flexible
    ########################

    #########################################################################
    # CHECK THE RETURN CONDITION OF `os.system` to verify if an error occured
    #########################################################################
    
    if tsv_name is None:
        tsv_name = "__NO_TSV__"
##
    # if arg_list is not list and arg_list is not tuple:
    #     raise ValueError("*** Error in restart_loop : `arg_list` must be an iterable collection of type list or tuple ***")
##
    if arg_list[0] is list or arg_list[0] is tuple:
        for i in range(len(arg_list)):
            arg_string = ""
            for elem in arg_list[i]:
                arg_string = arg_string + str(elem) + " "
            os.system(f"python {filename} {tsv_name} {i} {arg_string}")

    else:
        for i in range(len(arg_list)):
            os.system(f"python {filename} {tsv_name} {i} {arg_list[i]}")    


def switcher(func, arg=None):
    args_to_pass = []

    # if override is not None:
    #     if override is list:
    #         func(*override) 

    if arg is not None:
        if arg is not int or arg is not list or arg is not tuple:
            None
            ################
            # Raise an error about type
            ################ 
        else:
            None
            ################
            # Raise a catchall saying that arg= caused the error
            ################

    if arg is None:
        for elem in sys.argv[3:]:
            args_to_pass.append(elem)
    else:
        if arg is int:
            args_to_pass.append(sys.argv[arg+3])
        else:
            for i in arg:
                args_to_pass.append(sys.argv[i+3])

    func(*args_to_pass)

    return None


def write_to_tsv(data_to_save):
    if sys.argv[1] == "__NO_TSV__":
        None
        ###########################
        # Raise error about not having a tsv specified in the 
        ###########################
    
    if data_to_save is not list or data_to_save is not tuple:
        None
        ###########################
        # Raise error about type
        ###########################

    ######################################
    # DO A THING THAT SAVES TO TSV THANK YOU
    ######################################

    
    S = f'{sys.argv[2]}\t'
    for elem in data_to_save:
        S += f"{elem}\t"
    
    fout = open(sys.argv[1], 'a')
    fout.write(S+'\n')
    fout.close()

    