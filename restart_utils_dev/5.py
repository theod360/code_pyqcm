from pyqcm import *
# from pyqcm.spectral import *
from restart_utils import *

################## - Decide whether to use symmetry or not - ##################
###############################################################################

USE_CLUSTER_SYMMETRY = True

###############################################################################

 
if USE_CLUSTER_SYMMETRY:
    set_global_parameter("periodic") # sets the global parameter `periodic` to true
    new_cluster_model("clus", 12, 0, [[12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]]) # defines the cluster with a translationnal symmetry generator        
else:
    new_cluster_model("clus", 12, 0) # defines a cluster with no symmetry generators

add_cluster("clus", [0,0,0], [[i,0,0] for i in range(12)])
lattice_model("12sites", [[12,0,0]])

hopping_operator("t", [1,0,0], -1)
interaction_operator("U")
 
set_parameters("""
    t=1
    U=4
    mu=0.5*U
""")

def switch_target_sector(sector):
    set_target_sectors([str(sector)])

# switches target sector according to instructions received
switcher(switch_target_sector)

new_model_instance()

gs_energy = ground_state()
print(f"U=0 ---> {gs_energy}")

# Saves gs_energy to a tsv
write_to_tsv([gs_energy[0][0]])
