from restart_utils import restart_loop

# Loops over different target sectors
restart_loop("5.py", 
[
    "R0:N12:S0",
    "R1:N12:S0", 
    "R2:N12:S0",
    "R3:N12:S0", 
    "R4:N12:S0", 
    "R5:N12:S0",
    "R6:N12:S0",
    "R7:N12:S0", 
    "R8:N12:S0",
    "R9:N12:S0", 
    "R10:N12:S0", 
    "R11:N12:S0"
], 
tsv_name="5_results.tsv"
)