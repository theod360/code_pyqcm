from pyqcm import *
from pyqcm.spectral import *

# import model_4x1
# import model_graphene
import model_graphene_bath
# import model_rashba
# import model_superconduction

set_target_sectors(["R0:N4:S0"])
set_parameters("""
    U=0
    mu=0.5*U
    t=1
    V=0
""")

    # tb1_1 = 0.5
    # tb2_1 = 0.5
    # eb1_1 = 1
    # eb2_1 = -1

new_model_instance()

# DoS(10) # Works very well, nice to look at

# Fermi_surface(quadrant=False, plane="xy") # Works well, one of the first functions I got acquainted with

# G_dispersion(contour=True, inv=False)

# Luttinger_surface() # Works well

# dispersion() # Yep, really cool

# gap(np.array([[i,0,0] for i in np.linspace(0,1,20)]))

# mdc() #OOOOHHHHHH pretty colors

# momentum_profile("t") # Seems to work despite me being dumb

# segment_dispersion() # This is really nice, maybe should've used this in a couple of the notebooks

# spectral_function(path="graphene")

# spin_mdc(opt="spins") # Works well with model_rashba

# mdc_anomalous(self=False)

# spectral_function_Lehmann()

#######################################################################
#################### - Merge these two functions - ####################
#######################################################################

cluster_spectral_function(full=True, opt=None)
cluster_spectral_function(full=True, opt="self")
cluster_spectral_function(full=True, opt="hyb")
cluster_spectral_function(full=True, opt="hybs")

# hybridization_function()

#######################################################################
