from pyqcm import *

new_cluster_model("clus", 4, 0)
add_cluster("clus", [0,0,0], [[0,0,0], [1,0,0], [0,1,0], [1,1,0]])
lattice_model("2D_2x2", [[2,0,0],[0,2,0]])

interaction_operator("U")
hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)

anomalous_operator("Delta", [1,0,0], 1)
anomalous_operator("Delta", [0,1,0], -1)
