######################################################
#       2x2 cluster in a lattice
######################################################

from pyqcm import *

new_cluster_model("clus", 4, 0) # A simple 4 site cluster
add_cluster(
    "clus", 
    [0,0,0], 
    [
        [0,0,0],
        [1,0,0],
        [0,1,0],
        [1,1,0]
    ]
)
lattice_model("2D_2x2", [[2,0,0], [0,2,0]])

interaction_operator("U")
interaction_operator("V", link=[1,0,0], amplitude=1)
interaction_operator("V", link=[0,1,0], amplitude=1)
hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)
anomalous_operator("D", [1,0,0], 1)
anomalous_operator("D", [0,1,0], -1)