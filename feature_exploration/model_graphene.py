######################################################
#       1x2 cluster in a graphene lattice
######################################################

from pyqcm import *

new_cluster_model("clus", 2, 0)
add_cluster("clus", [0,0,0], [[0,0,0], [1,0,0]]) 
lattice_model("Graphene_2", [[1,-1,0], [2,1,0]], [[1,-1,0], [2,1,0]]) 
set_basis([[1,0,0],[-0.5,np.sqrt(3)/2,0]]) 

interaction_operator("U")
# interaction_operator("U", band1=1, band2=1) # interaction on band 1
# interaction_operator("U", band1=2, band2=2) # interaction on band 2
interaction_operator("V", link=[1,0,0], amplitude=1, band1=1, band2=2)
interaction_operator("V", link=[0,1,0], amplitude=1, band1=1, band2=2)
interaction_operator("V", link=[-1,-1,0], amplitude=1, band1=1, band2=2)
hopping_operator('t', [1,0,0], -1, band1=1, band2=2) 
hopping_operator('t', [0,1,0], -1, band1=1, band2=2)
hopping_operator('t', [-1,-1,0], -1, band1=1, band2=2)