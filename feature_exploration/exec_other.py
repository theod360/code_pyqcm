from pyqcm import *
from pyqcm.profile import plot_profile
from pyqcm.slab import slab

new_cluster_model("slab_clus", 4, 0)

slab_obj = slab(
    "2D_lat",
    3,
    "slab_clus",
    [
        [0,0,0],
        [1,0,0],
        [0,1,0],
        [1,1,0]
    ],
    [
        [2,0,0],
        [0,2,0]
    ],
    [
        [2,0,0],
        [0,2,0]
    ]
)

slab_obj.hopping_operator("t", [1,0,0], -1)
slab_obj.hopping_operator("t", [0,1,0], -1)

slab_obj.interaction_operator("U")
slab_obj.interaction_operator("U")

set_target_sectors(["R0:N4:S0", "R0:N4:S0", "R0:N4:S0"])
set_parameters("""
    U=4
    mu=0.5*U
    t=1
""")
new_model_instance()

averages()

# plot_profile() # Ok this is pretty cool despite me not knowing exactly what this does

