from pyqcm import *
from pyqcm.loop import *

import model_4x1

set_target_sectors(["R0:N4:S0"])
set_parameters("""
    U=4
    mu=0.5*U
    V=1
    t=1
""")

new_model_instance()
averages()

# ####################### - Fade - #######################
def _fade_func():
    print(parameters())

P1 = {
    "U" : 4,
    "V" : 1,
    "t" : 1,
}

P2 = {
    "U" : 2,
    "V" : 2,
    "t" : 2,
}

fade(_fade_func, P1, P2, 10)
# ########################################################

# fixed_density_loop # A hybrid between fade and controlled_loop???

# def _loop_func():
#     print(parameters())

# loop_from_file(_loop_func, "./averages.tsv")




#########################################################
###################### - Skipped - ######################
#########################################################

# fixed_density_loop

##############################################################
###################### - Tried & True - ######################
##############################################################

# Hartree # I've used this and it seems to work legit

# controlled_loop # I've used this and I have no real complaints

