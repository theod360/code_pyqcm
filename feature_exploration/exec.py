from pyqcm import *
import numpy as np
# import matplotlib.pyplot as plt

import model_4x1
# import model_2x2
# import model_graphene
# import model_graphene_bath
# import model_superconduction
# import model_rashba

# set_target_sectors(["R0:N4:S0"])
sectors(R=[0,0], N=[4,4], S=[0,2])
set_parameters("""
    U=4
    mu=0.5*U
    V=1e-9
    t=1e-4
""")

# Kind of a sanity check
new_model_instance()
print(ground_state(file="test.tsv"))
print(averages()["mu"])

print_model("model.out")

##############################################################################
#       Green function utilities
##############################################################################

# G = CPT_Green_function(0, np.array([0,0,0]))
# G_inv = CPT_Green_function_inverse(0, np.array([[0,0,0]]))

# print(G)
# print(G_inv)
# print(np.round(np.matmul(G, G_inv).astype("double"), decimals=1)) # This should really give diag{1,1,1,1}

# print(Green_function_dimension()) # Works extremely intuitively

# Green_function_solve() # This probably works exactly as intended, not worried

# Lehmann_G = Lehmann_Green_function(np.array([0,0,0]))
# print(Lehmann_G)

# print(QP_weight(np.array([0,0,0]))) #### STRANGE : has a tendency to interpret a 1d array of dim 3 as three vectors...

# print(V_matrix(1, np.array([np.pi/2,0,0]))) # Prints G_0^-1 - (G_0^c)^-1, gives an idea of how close both methods for calculating the Green's function are.

# print(band_Green_function(0, np.array([np.pi/2,0,0]), )) # Seems to work well with Graphene thing 2x2 matrix out

# print(cluster_Green_function(0, 0)) 

# print(cluster_Green_function_average()) # <--------------BROKEN

# print(cluster_Green_function_dimension())

# print(cluster_QP_weight()) # Not well versed in the physics, but seems to work

# print(hybridization_Lehmann()) # Checks if there are bath sites first

# print(hybridization_function(0,1+1j))

# print(periodized_Green_function(0,np.array([0,0,0]))) # This honestly works as expected

# M = periodized_Green_function(0,np.array([0,0,0]))
# print(M)

# for i in range(2):
#     for j in range(2):
#         print(periodized_Green_function_element(i, j, 0, np.array([0,0,0])))

# print(np.shape(projected_Green_function(0))) # I think this works...

# print(reduced_Green_function_dimension()) # this also seems to work so like... yeah

##############################################################################
#       Self-energy
##############################################################################

# Potthoff_functional() # Simply computes one point on the Potthoff functionnal, works as expected

# print(cluster_self_energy(0, 0)) # Seems to make sense and work... Gonna have to take some QFT

# print(self_energy(0,np.array([0,0,0])))

##############################################################################
#       Other
##############################################################################

# print(averages()) # This works, a dictionnary would be nice

# print(cluster_averages()) # Just works

# print(cluster_hopping_matrix())

# print(cluster_info())

print(cluster_parameters()[0]["mu"]) # Works exactly as expected, pretty basic stuff, actually uses a dictionnary

# print(dispersion(np.array([[i,0,0] for i in np.arange(-0.5, 1.1, 0.5)]))) # Works as expected, watch out for that [0,1[ wavevector grid

# for w in np.arange(-2,2,0.1): # seems to work??
#     print(dos(w))

# print(ground_state()) # This works immediately and intuitively

# print(interactions()) # returns (site1, site2, amplitude)

# print(matrix_elements("clus", "U"))

# print(mixing()) # SEEMS TO WORK

# print(model_is_closed()) # Gonna modify the True/False output value

# print(model_size()) # works exactly as expected from the doc

# print(parameter_set()) # all parameters is pretty useful and nifty

# print(parameter_string(CR=True))

# print(parameters()) # Really nothing to signal here...

# d = params_from_file("dos.tsv", n=0) # Mais c'est dont bien pratique!!
# print(d["up_1"])

# print_averages(averages())

# print_cluster_averages(cluster_averages()) # I have no clue if this is how it works

# print_graph("clus", [[0,0,0], [0,1,0], [1,0,0], [1,1,0]]) # Seems to work I mean yeah...

# print_model("clus") # tried and true, I've used this one a billion times

# print_options(opt=1)

# print_parameters(parameter_set()) # Seems to work a little bit better than printing the dictionnary

# print(properties()) # Finalement ça marches!!

# print(qmatrix())

# s = read_from_file("averages.tsv", n=0)
# print(s)

# set_params_from_file # Worked just after the parameterset

# print_wavefunction()
# print(print_wavefunction())

# print(np.shape(site_and_bond_profile()[1])) # Yep this works out yeahhh

# print(spatial_dimension()) # Works, there is no problem with this

# print(spectral_average("t", 0)) # seems to work, is this somewhat related to DoS

# print(spin_spectral_function(0, np.array([0,0,0])))

# print(susceptibility("t", 0))

# print(susceptibility_poles("m")) # seems legit

# print(tk(np.array([0,0,0])))

# print(np.shape(wavevector_grid())) # This works

# print(wavevector_path()) # This too

# write_cluster_instance_to_file("test.out") # This, of course, works

# ###################################################################### Show to Senech
# N=20

# k_grid = np.zeros((N, 3))
# k_grid[:,0] = np.linspace(0,1,N)

# print(np.shape(momentum_profile("t", k_grid)[:]))
# print(momentum_profile("t", k_grid)[:])

# plt.plot(k_grid[:,0], momentum_profile("t", k_grid), "o")
# plt.show()
# ###################################################################### Show to Senech

##############################################################################
#       Tried and true
##############################################################################

# hopping operator
# interaction_operator
# add_cluster
# set_basis
# set_parameter
# set_parameters
# set_target_sectors
# anomalous_operator
# density_wave
# explicit_operator
# new_cluster_operator()
# new_cluster_operator_complex()

###################################
########### - Skipped - ###########
###################################

# epsilon #################################### must ask Senech how this works...
# lattice_operator
# matrix_elements("clus", "U")############################################# segmentation fault...
# new_cluster_model_instance ######################################################## What does this even do?
# read_cluster_model_instance() # Je ne comprends pas comment ceci marche
# read_model("model.out")
# switch_cluster_model
# update_bath
# variational_parameters()

#########################################
#       On the fly
#########################################   

# print_wavefunction()
# print(model_is_closed())