from pyqcm import *
from pyqcm.vca import *

import model_2x2

set_target_sectors(["R0:S0"])

set_parameters("""
    t=1
    U=8
    mu=1.2
    D=1e-9
    D_1=0.1
""")

new_model_instance()

# plot_GS_energy("U", np.arange(1,7,0.5)) # This seems to work

# plot_sef("t", np.arange(1e-9,1,0.05), file="sef_test.tsv") # I've used this a million times, works super well ---> Hartree just works

vca(names=["D_1"], start=[0.15], steps=[0.001], accur=[1e-4], max=[20], file="vca_test.tsv") # Works as expected, hartree works from experience

# vca_min(names=["D_1"], steps=[0.001], accur=[1e-4], file="vca_test.tsv") # seems to be far less picky about starting values, (makes sense no?)

#################################################
################# - Questions - #################
#################################################

# transition_line # What is this?


