from pyqcm import *
from pyqcm.cdmft import *

import model_graphene_bath

set_target_sectors(["R0:N6:S0"])
set_parameters("""
    U=4
    V=1e-9
    mu=0.5*U
    t=1
    tb1_1 = 0.5
    tb2_1 = 0.5
    eb1_1 = 1
    eb2_1 = -1
""")

# cdmft(varia=["tb1_1", "tb2_1", "eb1_1", "eb2_1"], accur=1e-4) # This works and I've used it many times

# cdmft_forcing("U", [i for i in range(1,9)], varia=["tb1_1", "tb2_1", "eb1_1", "eb2_1"]) # Pretty sttrrange...

# forcing_sequence(8, 1, 1, 50, 19) # les arguments sont dans un ordre bizarre

# print(moving_std(np.arange(10,1,-1), 4)) # ok, I think it's kind of a deep cut

# obj = general_bath("clus", 2, 4) # This is the best

#################################################
################# - Questions - #################
#################################################

# cdmft_variational_sequence
# cdmft_distance_debug(varia=["tb1_1", "tb2_1", "eb1_1", "eb2_1"], vset=[[0.5],[0.5],[1],[-1]]) ################# What is this???
