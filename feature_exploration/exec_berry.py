from pyqcm import *
from pyqcm.berry import *

import model_graphene

set_target_sectors(["R0:N4:S0"])

set_parameters("""
    U=3
    mu=0.5*U
    t=1
    V=0
""")
new_model_instance()

Berry_curvature()

Berry_field_map()

print(Berry_flux(np.array([0,0,0]), 2))

print(Berry_flux_map(nk=200)) 

print(Chern_number(period="G")) 


##############################################
########## - Weyl-Semimetal stuff - ##########
##############################################

# monopole
# monopole_map

# Ok, I think that this works... maybe a TP would help?