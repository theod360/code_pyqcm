from pyqcm import *

new_cluster_model('L4', 4, 0)
add_cluster('L4', [0, 0, 0], [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0]])
lattice_model('1D_L4', [[2, 0, 0], [0, 2, 0]])

interaction_operator("U")
interaction_operator("V", link=[1,0,0], amplitude=1)
interaction_operator("V", link=[0,1,0], amplitude=1)
hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)
hopping_operator("m", [1,0,0], -1, tau=2, sigma=1)
hopping_operator("m", [0,1,0], -1, tau=2, sigma=1)