from pyqcm import *
import pyqcm.spectral as spectral
import model_2x2

sectors(R=0, N=4, S=0)
set_parameters("""
    U=4
    mu=0.5*U
    V=1e-9
    t=1e-4
""")

set_global_parameter("verb_Hilbert")

print(ground_state())