from tkinter import W
from pyqcm import * 
from pyqcm.hartree import *
from pyqcm.loop import Hartree

import model_4x1

# Constructing the interaction operator
explicit_operator(
    "Vm",
    [
        ([0,0,0], [0,0,0], 1/np.sqrt(2)),
        ([3,0,0], [0,0,0], 1/np.sqrt(2))
    ], 
    tau=0
) 

set_target_sectors(["R0:N4:S0"])
set_parameters("""
    U=4
    V=1
    Vm=1
    mu=4
    t=1
""")


def update_mu():
    new_model_instance()

    P = parameters()
    set_parameter("mu", 0.5*P["U"] + 2*P["V"])

    new_model_instance()


Vm_obj = hartree("Vm", "V", 1, lattice=True)

Hartree(new_model_instance, [Vm_obj]) # Yeahhhh this works!!

#######################################################
#################### - Questions - ####################
#######################################################

# counterterms # Comment est-ce que ça marche??