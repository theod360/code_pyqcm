# Nelder-Mead optimization wrapper

## `NM_wrapper.py`
Script containing the class `NM_wrapper` containing a method `cached_NM`, which essentially modifies `scipy.optimize.minimize` to prevent redundant function evaluations through the upkeep of an internal cache.

## `NM_supra_VCA.py`
Demo/test of `NM_wrapper.cached_NM` to show how it can be used to find the value of the chemical potential $\mu$ that maximizes the average value of the superconduction parameter for different values of $U$.

## Quick note
For whomever will touch this class, I recommend adding in a verbosity option that allows for outputs to be printed. Long calculations might appear not to work without it to the user !