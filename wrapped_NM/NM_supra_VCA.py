"""################################################

NM_supra_VCA.py

A quick example of an application of the wrapped
Nelder-Mead method to minimize function calls as
found in NM_wrapper.py

"""################################################

from pyqcm import *
from pyqcm.vca import *
from pyqcm.loop import controlled_loop

from NM_wrapper import NM_wrapper

import numpy as np
 
# Defining a simple 2x2 cluster and inserting into a square-tiled lattice
new_cluster_model("clus", 4, 0)
add_cluster("clus", [0,0,0], [[0,0,0], [1,0,0], [0,1,0], [1,1,0]])
lattice_model("2D_2x2", [[2,0,0],[0,2,0]])

# Adding the typical Hubbard style operators
interaction_operator("U")
hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)

# d_{x^2-y^2}-wave
anomalous_operator("D", [1,0,0], 1)
anomalous_operator("D", [0,1,0], -1)
 
# Defining the appropriate sector of Hilbert space
set_target_sectors(["R0:S0"])

# The first three parameters are used for the graph of the Potthoff functionnal
set_parameters("""
    t=1
    U=8
    mu=1.2
    D_1=1e-9
""")

# Setting parameters for a VCA around d_{x^2-y^2} superconduction
set_parameter("D_1", 0.15) # Starting parameter for the VCA
new_model_instance()


def get_order_param(mu_value):
    """Get the value of the order parameter as a function of the chemical potential.
    """
    set_parameter("mu", mu_value[0])
    new_model_instance()

    return vca(names=["D_1"], steps=[0.01], accur=[2e-3], max=[100], max_iter=60)[0][0]


wrap = NM_wrapper(maximize=True)
RESULTS = {}


def loop_func(U):
    """Function to execute within loop over values of U.
    """
    set_parameter("U", U)
    new_model_instance()

    wrap.clear_cache()

    return wrap.cached_NM(get_order_param, [1.6])


for U in range(8, 4, -1):
    print(f"******************************************* - U={U} - *******************************************")
    RESULTS[U] = loop_func(U).x

print(RESULTS)