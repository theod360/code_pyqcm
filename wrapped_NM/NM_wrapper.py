import os
import numpy as np
from scipy.optimize import minimize

class NM_wrapper:
    """Simple wrapper for the Nelder-Mead procedure with a cache to avoid full function recalculations.
    
    :param boolean maximize: Flips sign of given function so that N-M maximizes it, false by default.
    :param boolean autoclear: Determines whether or not to clear cache before each use, true by default.
    :param int simplex_print_freq: Determines how often to write to the simplex log file, 0 by default (no log).
    :param str simplex_print_file: Determines the name of the simplex log file, 'simplex_log.tsv' by default.
    :param boolean handle_file_already_exists: Makes sure not to output to already existing log file, True by default.
    :param str initial_simplex_from_file: Reads appropriate lines at end of file to use as a starting simplex, None by default.

    attributes:
        - NM_func_cache (dict) : A dictionnary of previous function evaluations.
    """
    def __init__(self, maximize=False, autoclear=True, simplex_print_freq=0, simplex_print_file="simplex_log.tsv", handle_file_already_exists=True, initial_simplex_from_file=None):
        # class variables
        self.maximize = maximize
        self.autoclear = autoclear # clears cache before each use if true
        self.simplex_print_freq = simplex_print_freq
        self.simplex_print_file = simplex_print_file
        self.handle_file_already_exists = handle_file_already_exists
        self.initial_simplex_from_file = initial_simplex_from_file

        # class attributes
        self.NM_func_cache = {}
        self.cache_reads = 0
        self.func_evals = 0
        
    def __repr__(self):
        return f"Number of items in cache --> {len(self.NM_func_cache)}\nNumber of cache reads --> {self.cache_reads}\nNumber of function evals --> {self.func_evals}"

    def _func_wrapper(self, func, X, args=None, kwargs=None, atol=1e-08, rtol=1e-05):
        """Implements internal cache that spits out values for points close enough to precalculated points.
        """
        X = tuple(X)

        # returns result with similar parameters if it exists
        for item in reversed(self.NM_func_cache.items()): # this is done backwards to 
            if np.allclose(item[0], X, atol, rtol):
                # print("Accessed internal cache !") # this can be used to diagnose 
                self.cache_reads += 1
                return item[1]

        if args is None and kwargs is None:
            f_val = func(X)
        elif args is not None and kwargs is None:
            f_val = func(X, *args)
        elif args is None and kwargs is not None:
            f_val = func(X, **kwargs)
        else:
            f_val = func(X, *args, **kwargs)    

        self.NM_func_cache[X] = f_val
        self.func_evals += 1
        return f_val
    
    def _minimize(self, wrapped_function, X0, minimize_kwargs, minimize_options):
        """Makes sure that minimize does not get unnecessary kwargs.
        """
        if minimize_kwargs is None:
            return minimize(wrapped_function, X0, method="Nelder-Mead", options=minimize_options)
        else:
            return minimize(wrapped_function, X0, method="Nelder-Mead", options=minimize_options, **minimize_kwargs)

    def _handle_file_already_exists(self):
        """Makes sure to append appropriate suffix to file in the case where it already exists
        """
        if os.path.isfile(self.simplex_print_file):
            print(f"***Warning : simplex log file ({self.simplex_print_file}) already exists.***")
            found_new_name = False
            name_index = 1
            while not found_new_name:
                new_name = f"{self.simplex_print_file[:-4]}_{name_index}.tsv"
                if os.path.isfile(new_name):
                    name_index += 1
                else:
                    self.simplex_print_file = new_name
                    found_new_name = True
                    print(f"***Now changing simplex log file to '{self.simplex_print_file}'.***")

    def _minimize_with_log(self, wrapped_function, X0, minimize_kwargs, minimize_options):
        """Writes progress to a simplex log file
        """
        old_name = self.simplex_print_file

        if self.handle_file_already_exists is True:
            self._handle_file_already_exists()
        N = len(X0)

        converged = False
        first_time = True
        minimize_options["maxiter"] = self.simplex_print_freq
        
        while not converged:
            if not first_time: 
                with open(self.simplex_print_file, "r") as outfile:
                    minimize_options["initial_simplex"] = np.genfromtxt(outfile, delimiter="\t")[-N-1:,:]
            result = self._minimize(wrapped_function, X0, minimize_kwargs, minimize_options)
            first_time = False
            converged = result.success
            simplex = result.final_simplex[0]
            with open(self.simplex_print_file, "a") as outfile:
                np.savetxt(outfile, simplex, delimiter="\t")
        self.simplex_print_file = old_name
        return result

    def cached_NM(self, func, X0, func_args=None, func_kwargs=None, cache_atol=1e-08, cache_rtol=1e-05, minimize_kwargs=None, minimize_options={}):
        """Performs a Nelder-Mead optimization routine withan internal function evaluation cache
        
        :param function func: function to optimize with the wrapped Nelder-Mead method.
        :param [float] X0: initial guess, same as in scipy.optimize.minimize.
        :param list func_args: non-parameter arguments to be passed to optimized function.
        :param dict func_kwargs: keyword arguments to be passed to function.
        :param float cache_atol: absolute tolerance for cached function evaluation parameter ressemblance.
        :param float cache_rtol: relative tolerance for cached function evaluation parameter ressemblance.
        :param dict minimize_kwargs: kwargs for the scipy.optimize.minimize function.
        :param dict minimize_options: options to pass to the scipy.optimize.minimize function.
        :return: normal return of the scipy.optimize.minimize function (OptimizeResult object).
        
        """
        if self.autoclear is True: # Executes the automatic cache clearing
            self.NM_func_cache = {}

        if self.maximize is True: # flips the sign of the function to optimize so as to maximize it
            wrapped_function = lambda X : -self._func_wrapper(
                func, 
                X, 
                args=func_args, 
                kwargs=func_kwargs, 
                atol=cache_atol, 
                rtol=cache_rtol
            )
        else:    
            wrapped_function = lambda X : self._func_wrapper(
                func, 
                X, 
                args=func_args, 
                kwargs=func_kwargs, 
                atol=cache_atol, 
                rtol=cache_rtol
            )
        
        if self.initial_simplex_from_file is not None:
            N = len(X0)
            with open(self.initial_simplex_from_file, "r") as outfile:
                    minimize_options["initial_simplex"] = np.genfromtxt(outfile, delimiter="\t")[-N-1:,:]

        if self.simplex_print_freq == 0:
            return self._minimize(wrapped_function, X0, minimize_kwargs, minimize_options)
        else:
            return self._minimize_with_log(wrapped_function, X0, minimize_kwargs, minimize_options)

    def clear_cache(self):
        """Clears the function evaluation cache"""
        self.NM_func_cache = {}


if __name__ == "__main__":
    """Quick example of the class
    """
    from time import sleep, time # to allow the artificial slowdown of the test function


    # testing function
    def test_gaussian(X):
        sleep(0.5)
        return -np.exp(-X[0]*X[0]-X[1]*X[1])

     
    wrap = NM_wrapper() # printing simplex to log every three iterations
    print(wrap.cached_NM(test_gaussian, [1,1])) # return of the minimization method

    print(wrap) # demonstration of the __repr__ method