from pyqcm import *
from pyqcm.spectral import *
import model_hex

set_target_sectors(["R0:N1:S1"])
set_parameters("""
    t=1
    U=1e-9
    mu=0.5*U
""")

new_model_instance()
spectral_function()
