from pyqcm import *
from pyqcm.spectral import *
from pyqcm.cdmft import *
import model_4S_8B

set_target_sectors(["R0:N4:S0"])
set_parameters("""
    t=1
    U=1e-9
    mu=0.5*U
    tb1_1=1
    tb2_1=1
    tbNN_1=1
    tbLoop_1=1
    eb1_1=1
    eb2_1=1
""")

cdmft(varia=["tb1_1", "tb2_1", "tbNN_1", "tbLoop_1", "eb1_1", "eb2_1"])