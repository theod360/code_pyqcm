from pyqcm import *
from pyqcm.draw_operator import *

ns = 4
nb = 8
nt = ns + nb

# Defining the cluster
new_cluster_model("clus", ns, nb)

# Defining the hopping and energy operators for all baths
new_cluster_operator("clus", "tb1", "one-body", [
    (1, 5, 1), 
    (2, 6, 1), 
    (3, 11, 1), 
    (4, 12, 1),
    (1+nt, 5+nt, 1), 
    (2+nt, 6+nt, 1), 
    (3+nt, 11+nt, 1), 
    (4+nt, 12+nt, 1)
])

new_cluster_operator("clus", "tb2", "one-body", [
    (1, 7, 1), 
    (2, 8, 1), 
    (3, 9, 1), 
    (4, 10, 1),
    (1+nt, 7+nt, 1), 
    (2+nt, 8+nt, 1), 
    (3+nt, 9+nt, 1), 
    (4+nt, 10+nt, 1)
])

new_cluster_operator("clus", "tbNN", "one-body", [
    (5, 6, 1),
    (8, 10, 1),
    (11, 12, 1),
    (7, 9, 1),
    (5+nt, 6+nt, 1),
    (8+nt, 10+nt, 1),
    (11+nt, 12+nt, 1),
    (7+nt, 9+nt, 1)
])

new_cluster_operator("clus", "tbLoop", "one-body", [
    (5, 11, 1),
    (6, 12, 1),
    (7, 8, 1),
    (9, 10, 1),
    (5+nt, 11+nt, 1),
    (6+nt, 12+nt, 1),
    (7+nt, 8+nt, 1),
    (9+nt, 10+nt, 1)
])

new_cluster_operator("clus", "eb1", "one-body", [
    (5, 5, 1), 
    (6, 6, 1), 
    (11, 11, 1), 
    (12, 12, 1),
    (5+nt, 5+nt, 1), 
    (6+nt, 6+nt, 1), 
    (11+nt, 11+nt, 1), 
    (12+nt, 12+nt, 1)
])

new_cluster_operator("clus", "eb2", "one-body", [
    (7, 7, 1), 
    (8, 8, 1), 
    (9, 9, 1), 
    (10, 10, 1),
    (7+nt, 7+nt, 1), 
    (8+nt, 8+nt, 1), 
    (9+nt, 9+nt, 1), 
    (10+nt, 10+nt, 1)
])

# Cluster and lattice geometry
add_cluster("clus", [0,0,0], [[0,0,0], [1,0,0], [0,1,0], [1,1,0]])
lattice_model("2x2_8B", [[2,0,0], [0,2,0]])

#Defining operators on the cluster
hopping_operator("t", [1,0,0], 1)
hopping_operator("t", [0,1,0], 1)
interaction_operator("U")

#Checking what the operators are like
print_model("model.out")
draw_operator("model.out", "t")
