"""
Error_handling.py

Author: Theo N. Dionne
Date: 2022-01-23

Just a fun little exercise to learn error handling.
"""


from argparse import ArgumentTypeError
from decimal import DivisionByZero


def function_accepting_string(string_argument):
    """A function that accepts a string and prints it.

    Args:
        string_argument (string): A given string.

    Returns:
        string_argument.

    Raises:
        ValueError: If string_argument is not a string
    """
    if type(string_argument) != str:
        raise ValueError(
            """********** Argument 'string_argument' must be of type 'str'! **********"""
        ) 

    return string_argument


#Here is a more complex example
def division(a, b):
    """A function that simply divides a by b...

    Args:
        a (float or int): Numerator in the division
        b (float or int): Denominator in the division

    Returns:
        The quotient of 'a' by 'b'

    Raises:
        ArgumentTypeError: When the input isn't floats or ints
        DivisionByZero: When b == 0
    """
    if not isinstance(a, (int, float)) or not isinstance(b, (int, float)):
        raise ArgumentTypeError("""
            ********** Arguments of division() must be of type 'int' or 'float' **********
        """)

    if b == 0:
        raise DivisionByZero("""
            ********** Denominator 'b' cannot be zero! **********
        """)

    return a/b


def main():
    print(division(1.2, 8.5))


if __name__ == "__main__":
    main()