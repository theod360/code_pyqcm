from pyqcm import *
import numpy as np

# Defining the geometry of the model
new_cluster_model("clus", 1, 0)
add_cluster("clus", [0,0,0], [[0,0,0]])
lattice_model("hex_1", [[1,0,0],[0,1,0]])
set_basis([[1,0,0],[-0.5,np.sqrt(3)/2,0]])

# Operators defined in the model
interaction_operator("U")
hopping_operator("t", [1,0,0], 1)
hopping_operator("t", [1,1,0], 1)
hopping_operator("t", [0,1,0], 1)