from pyqcm import *

new_cluster_model("4x2_clus", 8, 0)
add_cluster("4x2_clus", [0,0,0], 
    [
        [0,0,0], 
        [1,0,0], 
        [0,1,0], 
        [1,1,0], 
        [0,2,0], 
        [1,2,0], 
        [0,3,0], 
        [1,3,0]
    ]
)
lattice_model("4x2_AFM", [[2,0,0], [0,4,0]])

hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)
interaction_operator("U")
density_wave("M", "Z", [1,1,0]) # Spin density wave to induce antiferromagnetism

set_target_sectors(["R0:N8:S0"])