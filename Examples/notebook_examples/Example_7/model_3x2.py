from pyqcm import *

new_cluster_model("3x2_clus", 6, 0)
add_cluster("3x2_clus", [0,0,0], 
    [
        [0,0,0], 
        [1,0,0], 
        [0,1,0], 
        [1,1,0], 
        [0,2,0], 
        [1,2,0]
    ]
)
lattice_model("3x2_AFM", [[2,0,0], [1,3,0]])

hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)
interaction_operator("U")
density_wave("M", "Z", [1,1,0]) # Spin density wave to induce antiferromagnetism

set_target_sectors(["R0:N6:S0"])