from pyqcm import *

new_cluster_model("5x2_clus", 10, 0)
add_cluster("5x2_clus", [0,0,0], 
    [
        [0,0,0], 
        [1,0,0], 
        [0,1,0],
        [1,1,0], 
        [0,2,0], 
        [1,2,0], 
        [0,3,0], 
        [1,3,0], 
        [0,4,0], 
        [1,4,0]
    ]
)
lattice_model("5x2_AFM", [[2,0,0], [1,5,0]])

hopping_operator("t", [1,0,0], -1)
hopping_operator("t", [0,1,0], -1)
interaction_operator("U")
density_wave("M", "Z", [1,1,0])

set_target_sectors(["R0:N10:S0"]) # Spin density wave to induce antiferromagnetism