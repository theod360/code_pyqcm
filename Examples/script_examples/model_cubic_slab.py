from pyqcm import *
from pyqcm.slab import *
import numpy as np

N_LAYERS = 5

sites = [[0,0,0], [1,0,0], [0,1,0], [1,1,0]]
superlattice = [[2,0,0], [0,2,0]]
lattice = [[1,0,0], [0,1,0]]

new_cluster_model("clus", 4, 0)

# making a 5 layer slab based on a 2D square lattice
cubic_slab = slab(
    "lat", 
    N_LAYERS, 
    "clus", 
    sites,
    superlattice, 
    lattice, 
    thickness=2
)

# Defining some operators via the appropriate class method
cubic_slab.interaction_operator("U")

cubic_slab.hopping_operator("t", [1,0,0], -1)
cubic_slab.hopping_operator("t", [0,1,0], -1)

cubic_slab.hopping_operator("m", [0,0,1], -1)

print_model("model.out")