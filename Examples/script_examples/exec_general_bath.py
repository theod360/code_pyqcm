"""

Example use case of the general_bath class included with pyqcm.
Here, the class is used uniquely to generate the operators to be 
used in the model. This can be seen as an alternative way of ac-
complishing the same goal as in example 3 (Mott transition in 
graphene).

"""

# pyqcm related imports
from pyqcm import *
from pyqcm.cdmft import *
from pyqcm.loop import *

# other module imports
import os
import numpy as np
import matplotlib.pyplot as plt

# importing the pre-defined model
import model_general_bath

# setting target sector and parameters for half-filling
set_target_sectors(["R0:N6:S0"])
# setting initial values and defining dependencies to make calculations a bit easier
set_parameters("""
    U = 2
    mu = 0.5*U
    t = 1
    eb1_1 = 1
    eb2_1 = -1
    eb3_1 = 1*eb1_1
    eb4_1 = 1*eb2_1
    tb11_1 = 0.5
    tb21_1 = 0.5
    tb32_1 = 1*tb11_1
    tb42_1 = 1*tb21_1
""")

# Clearing the custom cdmft file if it exists in the working directory
if os.path.exists("./cdmft_gb.tsv"):
    os.remove("./cdmft_gb.tsv")


def loop_function():
    """Simply runs a cdmft over bath parameters and writes it to a custom cdmft file
    """
    cdmft(varia=["eb1_1", "eb2_1", "tb11_1", "tb21_1"], file="cdmft_gb.tsv")


# Looping the cdmft over a range of U values
controlled_loop(loop_function, varia=["eb1_1", "eb2_1", "tb11_1", "tb21_1"], loop_param="U", loop_range=(1e-3, 10, 0.2))

# Grabbing data from the output tsv
d = np.genfromtxt("./cdmft_gb.tsv", delimiter='\t', names=True)

# Plotting the variationnal parameters as a function of U.
fig, axs = plt.subplots(2, 1)

axs[0].plot(d["U"], d["eb1_1"], label="eb1_1")
axs[0].plot(d["U"], d["eb2_1"], label="eb2_1")
axs[0].legend()

axs[1].plot(d["U"], d["tb11_1"], label="tb11_1")
axs[1].plot(d["U"], d["tb21_1"], label="tb21_1")
axs[1].legend()

plt.show()