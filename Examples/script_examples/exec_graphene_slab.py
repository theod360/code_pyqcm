from pyqcm import *
import pyqcm.spectral as spectral

import model_graphene_slab as model_graphene_slab
from model_graphene_slab import N_LAYERS

target_sectors = ["R0:N2:S0" for i in range(N_LAYERS)] # creating as many cluster models 

set_target_sectors(target_sectors)
set_parameters("""
    U=1e-9
    mu=0.5*U
    t=1
    m=0.3
""")

spectral.spectral_function(band=1, path="graphene") # All the various band structures start to form a wider "single" band
