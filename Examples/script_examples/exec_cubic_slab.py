from pyqcm import *
import pyqcm.spectral as spectral

import model_cubic_slab
from model_cubic_slab import N_LAYERS

target_sectors = ["R0:N4:S0" for i in range(N_LAYERS)] # creating as many cluster models 

set_target_sectors(target_sectors)
set_parameters("""
    U=2
    mu=0.5*U
    t=1
    m=0.3
""")

spectral.spectral_function(band=1) # This should show a sort of 'band degeneracy' (for every layer there should be a visible structure)
