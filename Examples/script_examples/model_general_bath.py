from pyqcm import *
from pyqcm.cdmft import *

# Generating appropriate operators for the specified site arrangement
bath_obj = general_bath("clus", 2, 4, sites=[[1], [1], [2], [2]]) # 2 bath orbitals per site

add_cluster("clus", [0,0,0], [[0,0,0], [1,0,0]]) # Adding the cluster in
lattice_model("Graphene_2", [[1,-1,0], [2,1,0]], [[1,-1,0], [2,1,0]]) # Tiling like an old scissorgrid elevator
set_basis([[1,0,0],[-0.5,np.sqrt(3)/2,0]]) # Classic Graphene basis (for simplicity and graphical purposes)

# Defining the interaction operator on BOTH bands
interaction_operator('U', band1=1, band2=1)
interaction_operator('U', band1=2, band2=2)

# Defining NN hopping terms
hopping_operator('t', [1,0,0], -1, band1=1, band2=2) # All hops here are from one band to another
hopping_operator('t', [0,1,0], -1, band1=1, band2=2)
hopping_operator('t', [-1,-1,0], -1, band1=1, band2=2)

# #################### - To explore starting_values - ####################
# print(bath_obj.starting_values(e=(1,1), hyb=(0.5,0)))
# ########################################################################