from pyqcm import *
from pyqcm.slab import slab

import numpy as np

N_LAYERS=10

set_basis([[1,0,0],[-0.5,np.sqrt(3)/2,0]])
sites = [
    [0,0,0], 
    [1,0,0]
]
superlattice = [
    [1,-1,0], 
    [2,1,0]
]
lattice = superlattice

new_cluster_model("clus", 2, 0)

slab_model = slab(
    "model", 
    N_LAYERS, 
    "clus", 
    sites,
    superlattice, 
    lattice, 
    thickness=None
)

slab_model.interaction_operator("U")
slab_model.interaction_operator("V", link=[1,0,0], amplitude=1, band1=1, band2=2)
slab_model.interaction_operator("V", link=[0,1,0], amplitude=1, band1=1, band2=2)
slab_model.interaction_operator("V", link=[-1,-1,0], amplitude=1, band1=1, band2=2)
slab_model.hopping_operator('t', [1,0,0], -1, band1=1, band2=2) 
slab_model.hopping_operator('t', [0,1,0], -1, band1=1, band2=2)
slab_model.hopping_operator('t', [-1,-1,0], -1, band1=1, band2=2)
slab_model.hopping_operator('m', [0,0,1], -1)

print_model("model.out")