#!/bin/bash
#SBATCH --job-name=CDMFT_sid
#SBATCH --time=0-12:0:0
#SBATCH --account=def-dsenech
#SBATCH --cpus-per-task=4
#SBATCH --mem 8g
##SBATCH --partition c-apc
#SBATCH --mail-type=FAIL,END,BEGIN
#SBATCH --mail-user=theo.nathaniel.dionne@usherbrooke.ca
ulimit -c 0
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
python ~/code_pyqcm/simulations/cdmft_exec_reprod.py

