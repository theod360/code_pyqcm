from pyqcm import *
from pyqcm.cdmft import *
from pyqcm.loop import fade
import os
import sys

import model_2x2_2C_8b_C2v_L

current_file_dir = os.path.dirname(os.path.abspath(__file__)) # gets script path

# Using a format s.t. (e, tpd, tpp, tppp) for params from 'Weber et al.'
d_params = {
    "La2CuO4" : (2.61, 1.39, 0.640, 0.103),
    "Pb2Sr2YCu3O8" : (2.32, 1.30, 0.673, 0.160),
    "Ca2CuO2Cl2" : (2.21, 1.27, 0.623, 0.132), 
    "La2CaCu2O6" : (2.20, 1.31, 0.644, 0.152),
    "Sr2Nd2NbCu2O10" : (2.10, 1.25, 0.612, 0.144), 
    "Bi2Sr2CuO6" : (2.06, 1.36, 0.677, 0.153), 
    "YBa2Cu3O7" : (2.05, 1.28, 0.673, 0.150), 
    "HgBa2CaCu2O6" : (1.93, 1.28, 0.663, 0.187), 
    "HgBa2CuO4" : (1.93, 1.25, 0.649, 0.161), 
    "Sr2CuO2Cl2" : (1.87, 1.15, 0.590, 0.140), 
    "HgBa2Ca2Cu3O8_outer" : (1.87, 1.29, 0.674, 0.184),
    "HgBa2Ca2Cu3O8_inner" : (1.94, 1.29, 0.656, 0.167),
    "Tl2Ba2CuO6" : (1.79, 1.27, 0.630, 0.150), 
    "LaBa2Cu3O7" : (1.77, 1.13, 0.620, 0.188), 
    "Bi2Sr2CaCu2O8" : (1.64, 1.34, 0.647, 0.133), 
    "Tl2Ba2CaCu2O8" : (1.27, 1.29, 0.638, 0.140),
    "Bi2Sr2Ca2Cu3O10_outer" : (1.24, 1.32, 0.617, 0.159),
    "Bi2Sr2Ca2Cu3O10_inner" : (2.24, 1.32, 0.678, 0.198)
}

# Renormalization so that tpp = 1.0
for item in d_params.items():
    conversion_ratio = 1/item[1][2]
    param_tuple = tuple([round(conversion_ratio*item[1][i], 4) for i in range(4)])
    d_params[item[0]] = param_tuple

bath_parameter_names = [
    "sb1_1",
    "sb2_1",
    "sb3_1",
    "sb4_1",
    "sb5_1",
    "sb6_1",
    "sb7_1",
    "sb8_1",
    "eb1_1",
    "eb2_1",
    "eb3_1",
    "eb4_1",
    "eb5_1",
    "eb6_1",
    "eb7_1",
    "eb8_1",
    "tb1_1",
    "tb2_1",
    "tb3_1",
    "tb4_1",
    "tb5_1",
    "tb6_1",
    "tb7_1",
    "tb8_1"
]


def fade_material_params(material_name):
    try:
        out_d = {}
        param_tuple = d_params[material_name]

        out_d["e"] = param_tuple[0]
        out_d["tpd"] = param_tuple[1]
        out_d["tpp"] = param_tuple[2]
        out_d["tppp"] = param_tuple[3]

        return out_d
    except(KeyError):
        print(f"Error : '{material_name}' is not a material defined in this model.")


# non-bath parameters
parameter_set_string = """
    U=14
    mu=1e-9
    e=1e-9
    D=1e-9
    tpd=1e-9
    tpp=1e-9
    tppp=1e-9
"""

# adding in bath parameters
for name in bath_parameter_names:
    parameter_set_string += f"\n{name}=1e-9"

set_target_sectors(["R0:S0", "R0:N8:S0"])
set_parameters(parameter_set_string) # Initializing parameters

material = "Sr2CuO2Cl2" # seems to be a rather promising material as seen in cdmft_forced_U13p5to16p5

os.chdir(f"{current_file_dir}/cdmft_output/{sys.argv[2]}")


print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!! - Fading to {material} - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!") # Extremely necessary for debugging!

set_params_from_file(f"{current_file_dir}/cdmft_max_supra_sid.tsv") # setting starting params from file... at maximum supra
new_model_instance() # refreshes averages

initial_P = {
    "e" : 2.3,
    "tpd" : 2.1, 
    "tpp" : 1, 
    "tppp" : 0.2,
    "mu" : 9.45
}

material_P = fade_material_params(material)
material_P["mu"] = 9.5 + 0.1*int(sys.argv[1])


def fade_func():
    """Function to be run inside fade
    """
    cdmft(
        varia=bath_parameter_names, 
        file=f"final_mu_{9.45+int(sys.argv[1])}_fade.tsv", 
        grid_type="sharp", 
        method="CG",
        maxiter=100
    )
    new_model_instance()

    return None 


fade(fade_func, initial_P, material_P, 30)
