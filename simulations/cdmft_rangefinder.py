from pyqcm import *
from pyqcm.cdmft import *
from pyqcm.loop import controlled_loop
import os
import sys

import model_2x2_2C_8b_C2v_L

current_file_dir = os.path.dirname(os.path.abspath(__file__)) # gets script path
if not os.path.exists(f"{current_file_dir}/cdmft_output"): # creates cdmft_output if it doesn't exist
    os.mkdir(f"{current_file_dir}/cdmft_output")
os.chdir(f"{current_file_dir}/cdmft_output") # changing to current directory 

RANGE_INDEX = int(sys.argv[1])

# Using a format s.t. (e, tpd, tpp, tppp)
d_params = {
    "La2CuO4" : (2.61, 1.39, 0.640, 0.103),
    "Pb2Sr2YCu3O8" : (2.32, 1.30, 0.673, 0.160),
    "Ca2CuO2Cl2" : (2.21, 1.27, 0.623, 0.132), 
    "La2CaCu2O6" : (2.20, 1.31, 0.644, 0.152),
    "Sr2Nd2NbCu2O10" : (2.10, 1.25, 0.612, 0.144), 
    "Bi2Sr2CuO6" : (2.06, 1.36, 0.677, 0.153), 
    "YBa2Cu3O7" : (2.05, 1.28, 0.673, 0.150), 
    "HgBa2CaCu2O6" : (1.93, 1.28, 0.663, 0.187), 
    "HgBa2CuO4" : (1.93, 1.25, 0.649, 0.161), 
    "Sr2CuO2Cl2" : (1.87, 1.15, 0.590, 0.140), 
    "HgBa2Ca2Cu3O8_outer" : (1.87, 1.29, 0.674, 0.184),
    "HgBa2Ca2Cu3O8_inner" : (1.94, 1.29, 0.656, 0.167),
    "Tl2Ba2CuO6" : (1.79, 1.27, 0.630, 0.150), 
    "LaBa2Cu3O7" : (1.77, 1.13, 0.620, 0.188), 
    "Bi2Sr2CaCu2O8" : (1.64, 1.34, 0.647, 0.133), 
    "Tl2Ba2CaCu2O8" : (1.27, 1.29, 0.638, 0.140),
    "Bi2Sr2Ca2Cu3O10_outer" : (1.24, 1.32, 0.617, 0.159),
    "Bi2Sr2Ca2Cu3O10_inner" : (2.24, 1.32, 0.678, 0.198)
}

bath_parameter_names = [
    "sb1_1",
    "sb2_1",
    "sb3_1",
    "sb4_1",
    "sb5_1",
    "sb6_1",
    "sb7_1",
    "sb8_1",
    "eb1_1",
    "eb2_1",
    "eb3_1",
    "eb4_1",
    "eb5_1",
    "eb6_1",
    "eb7_1",
    "eb8_1",
    "tb1_1",
    "tb2_1",
    "tb3_1",
    "tb4_1",
    "tb5_1",
    "tb6_1",
    "tb7_1",
    "tb8_1"
]


def use_material(material_name):
    try:
        param_tuple = d_params[material_name]
        set_parameter("e", param_tuple[0])
        set_parameter("tpd", param_tuple[1])
        set_parameter("tpp", param_tuple[2])
        set_parameter("tppp", param_tuple[3])
    except(KeyError):
        print(f"Error : '{material_name}' is not a material defined in this model.")

# non-bath parameters
parameter_set_string = """
    U=14
    mu=1e-9
    e=1e-9
    D=1e-9
    tpd=1e-9
    tpp=1e-9
    tppp=1e-9
"""

# adding in bath parameters
for name in bath_parameter_names:
    parameter_set_string += f"\n{name}=1e-9"

set_target_sectors(["R0:S0", "R0:N8:S0"])
set_parameters(parameter_set_string) # Initializing parameters

material = list(d_params)[0] # Simulating "La2CuO4"

print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!! - Simulating {material} - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!") # Extremely necessary for debugging!

set_params_from_file(f"{current_file_dir}/cdmft_starting_state.tsv") # setting starting params from file
use_material(material) # choosing material on which to base simulation
new_model_instance() # refreshes averages


def run_cdmft():
    """Simply just runs a cdmft
    """
    global material
    cdmft(varia=bath_parameter_names, file=f"{material}_cdmft_{RANGE_INDEX}.tsv", wc=10) # setting the bath operators as the variationnal parameters


# Looping over values of U
controlled_loop(
    func=run_cdmft, 
    varia=bath_parameter_names, 
    loop_param="mu", 
    loop_range=(8+RANGE_INDEX, 9+RANGE_INDEX+0.005, 0.01)
)