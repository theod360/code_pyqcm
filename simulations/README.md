# IMPORTANT INSTRUCTIONS

## Context
In this directory are the files used to apply the 3 band Hubbard model to simulate various cuprates and observe the apparition of superconduction (here $\langle D\rangle$) as a function of the particle density $\langle\mu \rangle$ in a similar way to **Kowalski & Dash (2021)**. To vary $\langle\mu\rangle$, we can vary $\mu$ as is the case here. 

The parameter sets used for simulating the different cuprates are taken from **Weber *et al.* (2012)** and are : $\epsilon_d - \epsilon_p$, $t_{pd}$, $t_{pp}$ and $t_{pp}'$.

In my attempts, the strategy adopted to obtain a feasible starting state for each material was to use a starting state with maximum superconduction for a three band material and to *fade* the parameters gradually into the desired parameter set while hoping to maintain a superconducting solution with the CDMFT method. ***Caution*** : the three band hubbard operators are renormalized such that $t_{pp} = 1$.

## `cdmft_fade_with_mu.py`

This is a script used to take the starting state in `cdmft_max_supra_sid.tsv` (which in turn is from `cdmft_U14.tsv`) and gradually fade it into the parameters for $Sr_2CuO_2Cl_2$ since it appeared to exhibit relatively strong superconduction as seen in `data_vis.ipynb` (directory changes **will** be required). 

Launched with `fade_launch_loop.sh`; pay close attention to `sys.argv` in the script.

**A partially successful example of this can be found in the `winning_fade/` directory.**

## `cdmft_fade_from_Sr.py`

This script is very similar to the previous one, except that the parameter fade is carried from $Sr_2CuO_2Cl_2$ to the other ones as can be seen in .

Once again, this script can be launched through a tweaking of the `fade_launch_loop.sh` shell script, but special attention to `sys.argv` is required !

**A partially successful example of this can be found in the `partial_success/` directory.**

## `cdmft_exec.py`

This script is adapted to varying $\mu$ for a given material to plot $\langle D\rangle$ as a function of $\langle\mu\rangle$.

As can be surmised from previous file descriptions, this can be launched using `material_launch_loop.sh`.

## Various notices

Some files, such as `cluster_2x2_2C_8b_C2v_L.py` and `model_2x2_2C_8b_C2v_L.py` are necessary for the scripts, since they contain the model used for the simulations.

Also, there are old, semi-obsolete tries in `old_stuff`; this can be browsed at one's own risk...

**SOME DIRECTORIES WILL NEED TO BE CHANGED IN THE SUBMISSION SCRIPTS**

## Ending note

To whomever is continuing this project, I apologize for the mess this directory is... this project was a brief yet fierce project at the end of my internship. Have fun with this project!
