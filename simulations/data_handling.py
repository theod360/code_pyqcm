import numpy as np
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
print(SCRIPT_DIR)

data_dirs = os.listdir(f"{SCRIPT_DIR}/cdmft_output")
print(data_dirs)

for dir in data_dirs:
    try:
        arr = np.genfromtxt(f"{SCRIPT_DIR}/cdmft_output/{dir}/cdmft.tsv", names=True)
        print(len(arr["ave_D"]))
    except:
        print(f"Something went wrong with {dir} !")