#!/bin/bash
#SBATCH --job-name=cdmft_mu_7
#SBATCH --time=1-0:0:0
#SBATCH --account=def-dsenech
#SBATCH --cpus-per-task=4
#SBATCH --mem 8g
##SBATCH --partition c-apc
#SBATCH --mail-type=FAIL,END,BEGIN
#SBATCH --mail-user=theo.nathaniel.dionne@usherbrooke.ca
ulimit -c 0
#CAUTION: this is a temporary fix (should ideally rely on SLURM_CPUS_PER_TASK)
export OMP_NUM_THREADS=4
python ~/code_pyqcm/simulations/cdmft_fade_with_mu.py 7 mu_plus7
