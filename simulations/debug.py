import numpy as np
from pyqcm import *
set_global_parameter('nosym')
import model_2x2_2C_8b_C2v_L
from pyqcm.spectral import *


set_target_sectors(['R0:N4:S0', 'R0:N8:S0'])
set_parameters("""
U = 0
mu = 9
tpd = 2.1
tpp = 1
tppp = 0.2
e = 2.3
""")

new_model_instance()
# spectral_function(wmax=(-14, 4), nk = 8, offset=8)
segment_dispersion(path='triangle')