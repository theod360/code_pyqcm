#!/bin/bash
###################################################
SIMULATION_DIR=~/code_pyqcm/simulations ########### THIS WILL NEED TO BE CHANGED (AND OTHER THINGS)
###################################################

MATERIAL_LIST=(
    "La2CuO4"
    "Pb2Sr2YCu3O8"
    "Ca2CuO2Cl2"
    "La2CaCu2O6"
    "Sr2Nd2NbCu2O10" 
    "Bi2Sr2CuO6"
    "YBa2Cu3O7" 
    "HgBa2CaCu2O6"
    "HgBa2CuO4"
    "Sr2CuO2Cl2"
    "HgBa2Ca2Cu3O8_outer"
    "HgBa2Ca2Cu3O8_inner"
    "Tl2Ba2CuO6"
    "LaBa2Cu3O7" 
    "Bi2Sr2CaCu2O8" 
    "Tl2Ba2CaCu2O8"
    "Bi2Sr2Ca2Cu3O10_outer"
    "Bi2Sr2Ca2Cu3O10_inner"
)

MATERIAL=${MATERIAL_LIST[$1]}

if [ ! -d "$SIMULATION_DIR/cdmft_output" ]
then
    cd "$SIMULATION_DIR"
    mkdir "cdmft_output"
fi

if [ ! -d "$SIMULATION_DIR/cdmft_output/$MATERIAL" ]
then
    cd "$SIMULATION_DIR/cdmft_output"
    mkdir "$MATERIAL"
fi

cd "$SIMULATION_DIR/cdmft_output/$MATERIAL"

cat > submit.sh <<EOF
#!/bin/bash
#SBATCH --job-name=cdmft_$MATERIAL
#SBATCH --time=1-0:0:0
#SBATCH --account=def-dsenech
#SBATCH --cpus-per-task=4
#SBATCH --mem 8g
##SBATCH --partition c-apc
#SBATCH --mail-type=FAIL,END,BEGIN
#SBATCH --mail-user=theo.nathaniel.dionne@usherbrooke.ca
ulimit -c 0
#CAUTION: this is a temporary fix (should ideally rely on SLURM_CPUS_PER_TASK)
export OMP_NUM_THREADS=4
python ~/code_pyqcm/simulations/cdmft_fade_from_Sr.py $1
EOF

sbatch submit.sh