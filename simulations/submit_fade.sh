#!/bin/bash
###################################################
SIMULATION_DIR=~/code_pyqcm/simulations ########### THIS WILL NEED TO BE CHANGED (AND OTHER THINGS)
###################################################

MATERIAL="Sr2CuO2Cl2"

if [ ! -d "$SIMULATION_DIR/cdmft_output" ]
then
    cd "$SIMULATION_DIR"
    mkdir "cdmft_output"
fi

if [ ! -d "$SIMULATION_DIR/cdmft_output/mu_plus$1" ]
then
    cd "$SIMULATION_DIR/cdmft_output"
    mkdir "mu_plus$1"
fi

cd "$SIMULATION_DIR/cdmft_output/mu_plus$1"

cat > submit.sh <<EOF
#!/bin/bash
#SBATCH --job-name=cdmft_mu_$1
#SBATCH --time=1-0:0:0
#SBATCH --account=def-dsenech
#SBATCH --cpus-per-task=4
#SBATCH --mem 8g
##SBATCH --partition c-apc
#SBATCH --mail-type=FAIL,END,BEGIN
#SBATCH --mail-user=theo.nathaniel.dionne@usherbrooke.ca
ulimit -c 0
#CAUTION: this is a temporary fix (should ideally rely on SLURM_CPUS_PER_TASK)
export OMP_NUM_THREADS=4
python ~/code_pyqcm/simulations/cdmft_fade_with_mu.py $1 mu_plus$1
EOF

sbatch submit.sh